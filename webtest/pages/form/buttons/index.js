import {
    loadForm, init
} from "../util/formLoader.js";

await init();
await loadForm(false);
