import CustomElement from "../element/CustomElement.js";
// import TPL from "./ChevronLeftSymbol.js.html" assert {type: "html"};
import STYLE from "./ChevronLeftSymbol.js.css" assert {type: "css"};

export default class Symbol extends CustomElement {

    constructor() {
        super();
        this.shadowRoot.innerHTML = "◀";
        STYLE.apply(this.shadowRoot);
        /* --- */
    }

}

customElements.define("emc-symbol-chevron-left", Symbol);
