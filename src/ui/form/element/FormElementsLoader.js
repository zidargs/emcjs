// element / input
import "./input/action/ActionInput.js";
import "./input/boolorlogic/BoolOrLogicInput.js";
import "./input/code/CodeInput.js";
import "./input/color/ColorInput.js";
import "./input/grid/GridInput.js";
import "./input/hotkey/HotkeyInput.js";
import "./input/keyvaluelist/KeyValueListInput.js";
import "./input/list/ListInput.js";
import "./input/logic/LogicInput.js";
import "./input/number/NumberInput.js";
import "./input/password/PasswordInput.js";
import "./input/range/RangeInput.js";
import "./input/search/SearchInput.js";
import "./input/string/StringInput.js";
import "./input/switch/SwitchInput.js";
import "./input/text/TextInput.js";
// element / select
import "./select/image/ImageSelect.js";
import "./select/list/ListSelect.js";
import "./select/relation/RelationSelect.js";
import "./select/search/SearchSelect.js";
import "./select/simple/SimpleSelect.js";
import "./select/switch/SwitchSelect.js";
import "./select/token/TokenSelect.js";
