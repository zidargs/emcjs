import AbstractElement from "./abstract/AbstractElement.js";
import AbstractOneChildElement from "./abstract/AbstractOneChildElement.js";
import STYLE from "./styles/OperatorNot.css" assert {type: "css"};

const TPL_CAPTION = "NOT";
const REFERENCE = "not";

export default class OperatorNot extends AbstractOneChildElement {

    constructor() {
        super(REFERENCE, TPL_CAPTION);
        STYLE.apply(this.shadowRoot);
    }

    calculate(state = {}) {
        let value;
        const ch = this.children;
        if (ch[0]) {
            const val = ch[0].calculate(state);
            value = +!val;
        }
        this.shadowRoot.getElementById("header").setAttribute("value", value);
        return value;
    }

}

AbstractElement.registerReference(REFERENCE, OperatorNot);
customElements.define(`emc-logic-${REFERENCE}`, OperatorNot);
