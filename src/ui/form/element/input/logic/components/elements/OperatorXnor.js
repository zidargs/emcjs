import AbstractElement from "./abstract/AbstractElement.js";
import AbstractTwoChildrenElement from "./abstract/AbstractTwoChildrenElement.js";
import STYLE from "./styles/OperatorXnor.css" assert {type: "css"};

const TPL_CAPTION = "XNOR";
const REFERENCE = "xnor";

export default class LogicElement extends AbstractTwoChildrenElement {

    constructor() {
        super(REFERENCE, TPL_CAPTION);
        STYLE.apply(this.shadowRoot);
    }

    calculate(state = {}) {
        let value;
        const ch = this.childList;
        if (ch[0] != null) {
            const val = ch[0].calculate(state);
            value = +val;
        }
        if (ch[1] != null) {
            const val = ch[1].calculate(state);
            value = +!(value != +val);
        }
        this.shadowRoot.getElementById("header").setAttribute("value", value);
        return value;
    }

}

AbstractElement.registerReference(REFERENCE, LogicElement);
customElements.define(`emc-logic-${REFERENCE}`, LogicElement);
