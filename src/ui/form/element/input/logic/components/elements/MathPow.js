import AbstractElement from "./abstract/AbstractElement.js";
import AbstractTwoChildrenElement from "./abstract/AbstractTwoChildrenElement.js";
import STYLE from "./styles/Math.css" assert {type: "css"};

const TPL_CAPTION = "POWER";
const REFERENCE = "pow";

export default class LogicElement extends AbstractTwoChildrenElement {

    constructor() {
        super(REFERENCE, TPL_CAPTION);
        STYLE.apply(this.shadowRoot);
    }

    calculate(state = {}) {
        let value;
        const ch = this.childList;
        if (ch[0] != null) {
            const val = ch[0].calculate(state);
            const v = parseFloat(val);
            if (isNaN(v)) {
                this.shadowRoot.getElementById("header").setAttribute("value", "NaN");
                return 0;
            }
            value = v;
        }
        if (ch[1] != null) {
            const val = ch[1].calculate(state);
            const v = parseFloat(val);
            if (isNaN(v)) {
                this.shadowRoot.getElementById("header").setAttribute("value", "NaN");
                return 0;
            }
            value = value ** v;
        }
        this.shadowRoot.getElementById("header").setAttribute("value", value);
        return value;
    }

}

AbstractElement.registerReference(REFERENCE, LogicElement);
customElements.define(`emc-logic-${REFERENCE}`, LogicElement);
