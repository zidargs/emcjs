import I18nLabel from "../ui/i18n/I18nLabel.js";

class SettingsBuilder {

    build(window, options) {
        for (const ref in options) {
            const option = options[ref];
            const category = option.category;
            const label = option.label || ref;
            const desc = option.desc;
            switch (option.type) {
                case "string": {
                    window.addStringInput(category, ref, label, desc, option.default, option.visible, option.resettable);
                } break;
                case "number": {
                    const min = parseFloat(option.min);
                    const max = parseFloat(option.max);
                    window.addNumberInput(category, ref, label, desc, option.default, option.visible, option.resettable, min, max);
                } break;
                case "range": {
                    const min = parseFloat(option.min);
                    const max = parseFloat(option.max);
                    window.addRangeInput(category, ref, label, desc, option.default, option.visible, option.resettable, min, max);
                } break;
                case "check": {
                    window.addCheckInput(category, ref, label, desc, option.default, option.visible, option.resettable);
                } break;
                case "color": {
                    window.addColorInput(category, ref, label, desc, option.default, option.visible, option.resettable);
                } break;
                case "hotkey": {
                    window.addKeyInput(category, ref, label, desc, option.default, option.visible, option.resettable);
                } break;
                case "choice": {
                    window.addChoiceInput(category, ref, label, desc, option.default, option.visible, option.resettable, option.values);
                } break;
                case "list": {
                    window.addListSelectInput(category, ref, label, desc, option.default, option.visible, option.resettable, option.multiple, option.values);
                } break;
                case "button": {
                    window.addButton(category, ref, label, desc, option.visible, option.text, () => {
                        const event = new Event(option.event ?? "button");
                        event.action = option.action;
                        window.dispatchEvent(event);
                    });
                } break;
                case "caption": {
                    const captionEl = document.createElement("div");
                    captionEl.className = "option-caption";

                    const labelEl = I18nLabel.getLabel(label);
                    captionEl.append(labelEl);

                    window.addElements(category, captionEl);
                } break;
            }
        }
    }

}

export default new SettingsBuilder();
