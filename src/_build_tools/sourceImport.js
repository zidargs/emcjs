import fs from "fs";
import path from "path";
import {
    Transform
} from "stream";

const HTMLTemplatePath = "util/html/Template.js";
const HTMLGlobalStylePath = "util/html/GlobalStyle.js";

const LNBR_SEQ = /(?:\r\n|\n|\r)/g;
const IMPORT_SCRIPT = /^\s*import(?:\s+([a-zA-Z0-9_$]+)\s+from)?\s+"([^"]+)"\s*;?$/;
const IMPORT_ASSERT = /^\s*import\s+([a-zA-Z0-9_$]+)\s+from\s+"([^"]+)"\s+assert\s+\{\s*type:\s*"([^"]+)"\s*\}\s*;?$/;

function normalizePath(path) {
    return path.replace(/\\/g, "/");
}

function augmentFile(emcJSPrefix, sourcePath, fileContent) {
    const sourceDir = path.dirname(sourcePath);
    // lists
    const importedHTML = [];
    const importedCSS = [];
    const importedJSON = [];
    const importedJSONFolder = [];
    // analyzer
    const script = fileContent.split(LNBR_SEQ).map((string) => {
        // check asserted import
        const isAssertImport = IMPORT_ASSERT.exec(string);
        if (isAssertImport != null) {
            const name = isAssertImport[1];
            const contentPath = isAssertImport[2];
            const assertType = isAssertImport[3];
            switch (assertType) {
                case "html": {
                    importedHTML.push({
                        name, contentPath
                    });
                    return null;
                }
                case "css": {
                    importedCSS.push({
                        name, contentPath
                    });
                    return null;
                }
                case "json": {
                    importedJSON.push({
                        name, contentPath
                    });
                    return null;
                }
                case "json-folder": {
                    importedJSONFolder.push({
                        name, contentPath
                    });
                    return null;
                }
            }
            return string;
        }
        // check missing .js file extension
        const isScriptImport = IMPORT_SCRIPT.exec(string);
        if (isScriptImport != null) {
            const name = isScriptImport[1];
            const filePath = isScriptImport[2];
            if (!filePath.endsWith(".js")) {
                // TODO check if folder or file
                // - file relative check if starting with "."
                // - project relative check if starting with "/"
                if (name == null) {
                    return `import "${filePath}.js";`;
                }
                return `import ${name} from "${filePath}.js";`;
            }
        }
        return string;
    }).filter((e) => e != null).join("\n");

    let result = "";
    if (importedCSS.length || importedHTML.length || importedJSON.length || importedJSONFolder.length) {
        result += "/* ===================== AUTOGENERATED SOURCE IMPORT ===================== */\n\n";

        // HTML
        if (importedHTML.length) {
            // import Template module
            const modulePath = normalizePath(`${emcJSPrefix}/${HTMLTemplatePath}`);
            result += `import Template from "${modulePath}";\n\n`;
            // include files
            for (const {name, contentPath} of importedHTML) {
                const resolvedPath = path.resolve(sourceDir, contentPath);
                // if the file exists
                if (fs.existsSync(resolvedPath)) {
                    const fileContent = fs.readFileSync(resolvedPath).toString();
                    result += `const ${name} = new Template(\`\n${fileContent.replace(/\\/g, "\\\\").split("\n").map((l) => `    ${l}`).join("\n")}\n\`);\n`;
                }
            }
            result += "\n";
        }

        // CSS
        if (importedCSS.length) {
            // import GlobalStyle module
            const modulePath = normalizePath(`${emcJSPrefix}/${HTMLGlobalStylePath}`);
            result += `import GlobalStyle from "${modulePath}";\n\n`;
            // include files
            for (const {name, contentPath} of importedCSS) {
                const resolvedPath = path.resolve(sourceDir, contentPath);
                // if the file exists
                if (fs.existsSync(resolvedPath)) {
                    const fileContent = fs.readFileSync(resolvedPath).toString();
                    result += `const ${name} = new GlobalStyle(\`\n${fileContent.replace(/\\/g, "\\\\").split("\n").map((l) => `    ${l}`).join("\n")}\n\`);\n`;
                }
            }
            result += "\n";
        }

        // JSON
        if (importedJSON.length) {
            // include files
            for (const {name, contentPath} of importedJSON) {
                const resolvedPath = path.resolve(sourceDir, contentPath);
                // if the file exists
                if (fs.existsSync(resolvedPath)) {
                    const fileContent = fs.readFileSync(resolvedPath).toString();
                    result += `const ${name} = ${fileContent.replace(/\\/g, "\\\\")};\n`;
                }
            }
            result += "\n";
        }

        // JSON-folder
        if (importedJSONFolder.length) {
            // include folders
            for (const {name, contentPath} of importedJSONFolder) {
                const resolvedFolderPath = path.resolve(sourceDir, contentPath);
                // if the folder exists
                if (fs.existsSync(resolvedFolderPath)) {
                    const typeDefs = [];
                    const folderContent = fs.readdirSync(resolvedFolderPath);
                    for (const filePath of folderContent) {
                        const resolvedFilePath = path.resolve(resolvedFolderPath, filePath);
                        // if the file exists
                        if (fs.existsSync(resolvedFilePath)) {
                            const parsedPath = path.parse(filePath);
                            const name = parsedPath.name;
                            const fileContent = fs.readFileSync(resolvedFilePath).toString();
                            try {
                                JSON.parse(fileContent);
                                typeDefs.push(`    ${name}: ${fileContent.replace(/\\/g, "\\\\").split("\n").map((l) => `    ${l}`).join("\n").slice(4)}`);
                            } catch {
                                // ignore
                            }
                        }
                    }
                    result += `const ${name} = {\n${typeDefs.join(",\n")}\n};\n`;
                }
            }
            result += "\n";
        }

        result += "/* ======================================================================= */\n\n";
    }
    return result + script;
}

export default function sourceImport(emcJSPrefix = "/emcJS") {
    // augment
    const transformStream = new Transform({objectMode: true});
    transformStream._transform = function(file, encoding, callback) {
        const contents = augmentFile(emcJSPrefix, file.path, String(file.contents));
        if (file.isBuffer() === true) {
            file.contents = Buffer.from(contents);
        } else {
            file.contents = contents;
        }
        callback(null, file);
    };
    return transformStream;
}
